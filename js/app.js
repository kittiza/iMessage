var FB = new Facebook();
var tokenList = [];
// var deleteMode = false;

function login(token,deleteMode) {
    if (deleteMode) {
        $("#row-" + token).fadeOut(400, function () {
            $(this).remove();
            if ($('#token_table tr').length == 0) {
                $('#token_table').append("<tr><td >Not have any access token</td></tr>");
            }
        });
        tokenList = JSON.parse(localStorage.TokenList);
        tokenList.splice(tokenList.indexOf(token), 1);
        localStorage.TokenList = JSON.stringify(tokenList);
    } else {
        sessionStorage.Token = token;
        window.location.href = "messages.html";
    }
}

function loadaccessToken() {
    var access_token = $("#access_token").val();
    FB.setAccessToken(access_token);
    FB.api("/me?fields=id,name", function (res) {
        if (res.error) {
            $.growl({
                message: 'Access token expire!'
            }, {
                type: 'danger',
                mouse_over: "pause",
                placement: {
                    from: "top",
                    align: "center"
                },
                allow_dismiss: false,
            })
        } else {
            var notin = true;
            tokenList.forEach(function (q) {
                if (q == access_token) {
                    notin = false;
                }
            });
            if (notin) {
                tokenList.push(access_token);
                localStorage.TokenList = JSON.stringify(tokenList);
            }
            login(access_token);
        }
    });
}

function toggle_delete() {
    if (!deleteMode) {
        deleteMode = true;
        $(".tokenlink").css({
            'color': '#d9534f'
        });
        $("#trashbutton").css({
            'color': '#d9534f'
        });
        $("#trashbutton").addClass("fa-spin");
    } else {
        deleteMode = false;
        $(".tokenlink").css({
            'color': '#428bca'
        });
        $("#trashbutton").css({
            'color': '#428bca'
        });
        $("#trashbutton").removeClass("fa-spin");
    }
}

function pagemain() {
    if (localStorage.TokenList && JSON.parse(localStorage.TokenList).length > 0) {
        tokenList = JSON.parse(localStorage.TokenList);
        tokenList.forEach(function (q) {
            FB.setAccessToken(q);
            FB.api('/me?fields=id,name', function (res) {
                if (!res.error && res.access_token != undefined) {
                    $("#token_table").append("<tr id='row-" + res.access_token + "'><td><div onclick='javascript:login(\"" + res.access_token +
                        "\")' class='tokenlink'> </div><img src='https://graph.facebook.com/" + res.id +
                        "/picture' class='img-responsive'  style='display: inline'></img> " + res.name + "</a></td><td><a href='javascript:login(\"" + res.access_token + "\")' class='btn btn-info'> Login</a></td><td><a href='javascript:login(\"" + res.access_token + "\",true)' class='btn btn-danger'> Delete</a></td></tr>");
                }
            });
        });
    } else {
        $(function () {
            $("#token_table").html("<tr><td colspan='2'>Not have any access token</td></tr>")
        });
    }
}
pagemain();
