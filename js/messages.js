var FB = new Facebook();
var user_id = "";
function getURL(name, url) {
    if (!url) {
        url = window.location.href;
    }
    var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(url);
    if (!results) { 
        return 0; 
    }
    return results[1] || 0;
}     
function display_date(date_str){
    function dateFromString(str) {
        console.log(str);
        var a = $.map(str.split(/[^0-9]/), function(s) { return parseInt(s, 10) });
        return new Date(a[0], a[1]-1 || 0, a[2] || 1, a[3] || 0, a[4] || 0, a[5] || 0, a[6] || 0);
    }
    function pad(n){return n<10 ? '0'+n : n}
    mdate = dateFromString(date_str);
    mdate.setTime(mdate.getTime()-mdate.getTimezoneOffset()*60*1000 );
    return mdate.getDate()+"/"+(mdate.getMonth()+1)+"/"+mdate.getFullYear()+"<br>"+pad(mdate.getHours())+":"+pad(mdate.getMinutes());
}
function action_loadmore_user(until = false){
    $("#loadmore-txt").remove();
    $("#user-display").append("<tr id='loading-spin'><td></td><td><center><i class='fa fa-refresh fa-spin'></i></center></td><td></td></tr>");
    if (until !== false) {
        var url = "/me/threads?pretty=0&fields=id%2Cparticipants%2Cpaging%2Cupdated_time&limit=25&after="+until;
    }else{
        var url = "/me/threads?fields=id,participants,updated_time&limit=25";
    }
    FB.api(url,function(res){
        console.log(res);
        var txt="";
        res.data.forEach(function(q){
            if(!q.participants||q.participants.data.length==1){
                txt+="<tr><td><i class='fa fa-user fa-3x'></i></td><td><a href='messages_t.html?id="+q.id+"'>Facebook User</a></td><td><a href='messages_t.html?id="+q.id+"' class='btn btn-info'> View</a></td><td>"+display_date(q.updated_time)+"</td>";
            }else if(q.participants.data.length==2){
                if(q.participants.data[0].id!=user_id){
                    txt+="<tr><td><img src='https://graph.facebook.com/"+q.participants.data[0].id+"/picture' class='img-responsive'></td><td>"+q.participants.data[0].name+"</td><td><a href='messages_t.html?id="+q.id+"' class='btn btn-info'> View</a></td><td>"+display_date(q.updated_time)+"</td>";
                }else{
                    txt+="<tr><td><img src='https://graph.facebook.com/"+q.participants.data[1].id+"/picture' class='img-responsive'></td><td>"+q.participants.data[1].name+"</td><td><a href='messages_t.html?id="+q.id+"' class='btn btn-info'> View</a></td><td>"+display_date(q.updated_time)+"</td>";						
                }
            }else {
                txt+="<tr><td><i class='fa fa-users fa-3x'></i></td><td>";
                q.participants.data.forEach(function(r){
                    if(r.id!=user_id){
                        txt+=r.name+", ";
                    }
                });
                txt = txt.substring(0, txt.length - 2);
                txt+="</td><td><a href='messages_t.html?id="+q.id+"' class='btn btn-info'> View</a></td><td>"+display_date(q.updated_time)+"</td>";
            }
        })
        if(typeof res.paging  !== 'undefined' &&typeof res.paging.next  !== 'undefined'){
            console.log(res.paging.next);
            txt+="<tr id='loadmore-txt'><td></td><td><a href=\"javascript:action_loadmore_user('"+res.paging.cursors.after+"')\"><i class='fa fa-history'></i> Load more</a></td><td></td></tr></tr>";
        } else {
            $.growl({
                message: '<b>All</b> contract have been loaded!'
            },{
                type: 'info',
                mouse_over: "pause",
                placement: {
                    from: "top",
                    align: "center"
                },
                allow_dismiss: false,
            })
        }
        $("#loading-spin").remove();
        $("#user-display").append(txt);
    });
}
function logout(){
    sessionStorage.removeItem("Token");
    window.location.href = "index.html";
}
function pagemain(){
    var token = getURL("token");
    if(token){
        sessionStorage.Token = token;
    }
    if(!sessionStorage.Token){
        window.location.href = "index.html";
    }
    FB.setAccessToken(sessionStorage.Token);
    FB.api("/me?fields=id,name",function(res){
        if(res.error){
            window.location.href = "index.html?error=token+expire";
        }else{
            user_id=res.id;
            $("#user-name").html("<a href='https://www.facebook.com/"+res.id+"'><img src='https://graph.facebook.com/"+res.id+"/picture' width='20px' height='20x'  style='display: inline'></img> "+res.name+"</a>");
        }
    })
    action_loadmore_user();
}
pagemain();